package chibde.com;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Canvas;
import android.util.AttributeSet;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;

/**
 * Created by gautam on 4/12/17.
 */

public class EditorView extends RelativeLayout {

    public EditorView(Context context) {
        super(context);
    }

    public EditorView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public EditorView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @SuppressLint("ClickableViewAccessibility")
    public void addImageView(ImageView view) {
        view.setOnTouchListener(new TouchEvent());
        view.setScaleType(ImageView.ScaleType.MATRIX);
        RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.MATCH_PARENT);
        view.setLayoutParams(params);
        this.addView(view);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
    }
}
